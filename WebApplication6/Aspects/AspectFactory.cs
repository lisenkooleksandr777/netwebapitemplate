﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Aspects
{
  public class AspectFactory
  {
    private static ILogger<HttpTolerantMetricAspect> logger;
     
    public AspectFactory(ILogger<HttpTolerantMetricAspect> l)
    {
      AspectFactory.logger = l;
    }

    public static object GetInstance(Type aspectType)
    {
      // Here you can implement any instantiation approach, this one is shown for the sake of simplicity
      if (aspectType == typeof(HttpTolerantMetricAspect))
      {
        return new HttpTolerantMetricAspect();
      }
      throw new ArgumentException($"Unknown aspect type '{aspectType.FullName}'");
    }
  }
}
