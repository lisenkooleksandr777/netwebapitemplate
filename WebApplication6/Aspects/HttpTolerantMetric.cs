﻿using AspectInjector.Broker;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace API.Aspects
{
  [Injection(typeof(HttpTolerantMetricAspect))]
  public sealed class HttpTolerantMetricAttribute : Attribute
  {
  }


  [Aspect(Scope.Global, Factory = typeof(AspectFactory))]
  public sealed class HttpTolerantMetricAspect
  {
    public HttpTolerantMetricAspect()
    {
    }

    [Advice(Kind.Around, Targets = Target.Method)]
    public object Trace(
       [Argument(Source.Type)] Type type,
       [Argument(Source.Name)] string name,
       [Argument(Source.Target)] Func<object[], object> methodDelegate,
       [Argument(Source.Arguments)] object[] args)
    {
      var stopWatch = new Stopwatch();
      stopWatch.Start();
      try
      {
        return methodDelegate(args);
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);//dd  
        //logger.LogError(e.Message);
        return 500;
      }
      finally
      {
        stopWatch.Stop();
        Console.WriteLine($"{name} roundtrip time is: {stopWatch.ElapsedMilliseconds}");
        ///logger.LogError($"{name} roundtrip time is: {stopWatch.ElapsedMilliseconds}");
      }
    }
  }
}
