﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using API.Aspects;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services.RandomNumberGeneration.Abstractions;

namespace WebApplication6.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class RandController : ControllerBase
  {
    private readonly IRandomNumberGenerator generator;
    
    public RandController(IRandomNumberGenerator generator)
    {
      this.generator = generator;
    }

    [HttpTolerantMetric]
    [HttpGet]
    public ActionResult<int> Get([FromQuery] int min, [FromQuery]int max)
    {
      return Ok(
          generator.GenerateRandomNumber(new Services.RandomNumberGeneration.Models.Input
          {
            Min = min,
            Max = max,
          }));
    }

    [HttpTolerantMetric]
    [HttpGet("anotherget")]
    public ActionResult<int> AnotherGet()
    {
      return Ok(
          generator.GenerateRandomNumber(new Services.RandomNumberGeneration.Models.Input
          {
            Min = 0,
            Max = 199,
          }));
    }

    ~RandController()
    {
      Console.WriteLine();
    }
  }
}
