﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace WebApplication6
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      Repository.Module.Init();
      Services.Module.Init();

      foreach (var dep in IoC.IoC.GetSingletons())
        services.AddSingleton(dep.Key, dep.Value);

      foreach (var dep in IoC.IoC.GetScopes())
        services.AddScoped(dep.Key, dep.Value);

      foreach (var dep in IoC.IoC.GetTransinets())
        services.AddTransient(dep.Key, dep.Value);

      services.AddLogging();

      services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    [Obsolete]
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {      
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseRouting();

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllerRoute(
          name: "default",
          pattern: "{controller=swagger}/{action=Index}/{id?}");
      });

    }
  }
}
