﻿using IoC;
using Services.RandomNumberGeneration.Abstractions;
using Services.RandomNumberGeneration.Implementations;

namespace Services
{
  public static class Module
  {
    public static void Init()
    {
      //RandomNumberGenerator: IRandomNumberGenerator
      IoC.IoC.AddScopped<IRandomNumberGenerator, RandomNumberGenerator>();
    }
  }
}
