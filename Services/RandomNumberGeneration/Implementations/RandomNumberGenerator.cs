﻿using Domain.Repositories;
using Services.RandomNumberGeneration.Abstractions;
using Services.RandomNumberGeneration.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.RandomNumberGeneration.Implementations
{
  public class RandomNumberGenerator : IRandomNumberGenerator
  {
    private readonly IRandomGenerator generator;

    public RandomNumberGenerator(IRandomGenerator generator)
    {
      this.generator = generator;
    }

    //Use Case 
    public int GenerateRandomNumber(Input input)
    {      
      var result = generator.Random(input);

      if (!result.IsSucces)
      {
        return -1;
      }
      else
      {
        return result.Result;
      }
    }
  }
}
