﻿using Services.RandomNumberGeneration.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.RandomNumberGeneration.Abstractions
{
  public interface IRandomNumberGenerator
  {
    public int GenerateRandomNumber(Input input);
  }
}
