﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.RandomNumberGeneration.Models
{
  public class Input : IInput
  {
    public int Min { get; set; }
    public int Max { get; set; }
  }
}
