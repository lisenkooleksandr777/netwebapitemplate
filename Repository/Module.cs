﻿using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository
{
  public static class Module
  {
    public static void Init()
    {
      IoC.IoC.AddScopped<IRandomGenerator, RandomGenerator>();
    }
  }
}
