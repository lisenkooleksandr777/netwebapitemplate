using NUnit.Framework;
using Repository;
using Services.RandomNumberGeneration.Implementations;
using static Tests.Mocks.Repositories.RandomNumberGeneratorMock;

namespace Tests
{
  public class Tests
  {
    [SetUp]
    public void Setup()
    {
    }

    //Fast: The code seems to be fast because there is nothing complex about its tests.
    //Independent: The test doesn't depend on other tests.
    //Repeatable: The test will get the same result every time. 'special_index' if it's Tuesday and 'index' if it's not Tuesday.
    //Self-checking: The test can automatically detect if it's passed.
    //Timely: Both the code and the test code are presented here at the same time.

    [Test]
    public void Test1()
    {
      //Arange
      var g = new RandomGenerator();

      //Act
      var result = g.Random(new InputMock { Min = 0, Max = 1 });

      //Assert
      Assert.AreNotEqual(-1, result.Result);
    }
    
    [Test]
    public void Test()
    {
      //Arange
      var g = new RandomGenerator();
      var service = new RandomNumberGenerator(g);
      
      //Act
      var result = service.GenerateRandomNumber(new Services.RandomNumberGeneration.Models.Input { Min = 0, Max = 1 });

      //Assert
      Assert.AreNotEqual(-1, result);
    }
  }
}