﻿using Domain.Models;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Mocks.Repositories
{
  public class RandomNumberGeneratorMock : IRandomGenerator
  {
    public IOutput<int> Random(IInput input)
    {
      return new OutputMock { 
        IsSucces = true, 
        Result = new Random().Next(input.Min, input.Max) 
      };
    }

    public class OutputMock : IOutput<int>
    {
      public bool IsSucces { get; set; }
      public int Result { get; set; }
    }

    public class InputMock : IInput
    {
      public int Min { get; set; }
      public int Max { get; set; }
    }
  }
}
