﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
  public interface IOutput<T>
  {
    public bool IsSucces { get; set; }
    public T Result { get; set; }    
  }

  public class Output<T> : IOutput<T>
  {
    public bool IsSucces { get; set; }
    public T Result { get; set; }
  }
}
