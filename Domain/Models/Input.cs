﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
  public interface IInput
  {
    public int Min { get; set; }
    public int Max { get; set; }
  }
}
